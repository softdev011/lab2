/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab2;

import java.util.Scanner;

/**
 *
 * @author growt
 */
public class Lab2 {
    private char[][] board;
    private char currentPlayer;
    
    public Lab2() {
        board = new char[3][3];
        currentPlayer = 'X';
        setBoard();
    }
    
    public void setBoard() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                board[row][col] = '-';
            }
        }
    }
    public void printWelcome() {
        System.out.println("Welcome to XO Game");
    }
    public void printBoard() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                System.out.print(board[row][col]+ " ");
            } System.out.println();
        }
    }
    public boolean isTie() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                if (board[row][col] == '-') {
                    return false;
                }
            }
        }
        return true;
    }
    public void changePlayer() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }
     public void makeMove(int row, int col) {
        if (row >= 0 && row < 3 && col >= 0 && col < 3 && board[row][col] == '-') {
            board[row][col] = currentPlayer;
            changePlayer();
        } else {
            System.out.println("Invalid move! Try again.");
        }
     }
     public boolean isWinner() {
        // Check rows
        for (int row = 0; row < 3; row++) {
            if (board[row][0] != '-' && board[row][0] == board[row][1] && board[row][1] == board[row][2]) {
                return true;
            }
        }
        
        // Check columns
        for (int col = 0; col < 3; col++) {
            if (board[0][col] != '-' && board[0][col] == board[1][col] && board[1][col] == board[2][col]) {
                return true;
            }
        }
        
        // Check diagonals
        if (board[0][0] != '-' && board[0][0] == board[1][1] && board[1][1] == board[2][2]) {
            return true;
        }
        if (board[0][2] != '-' && board[0][2] == board[1][1] && board[1][1] == board[2][0]) {
            return true;
        }
        
        return false;
    }

    public static void main(String[] args) {
        Lab2 game = new Lab2();
        Scanner scanner = new Scanner(System.in);
        game.printWelcome();
        while (!game.isTie() && !game.isWinner()) {
            game.printBoard();
            
            System.out.print("Player " + game.currentPlayer + ", enter your move (row[0-2] col[0-2]): ");
            int row = scanner.nextInt();
            int col = scanner.nextInt();
            
            game.makeMove(row, col);
        }
        
        game.printBoard();
        
        if (game.isWinner()) {
            game.changePlayer();
            System.out.println("Player " + game.currentPlayer + " wins!");
        } else {
            System.out.println("It's a tie!");
        }
        
        scanner.close();
    }
}
